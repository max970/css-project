import argparse
import networkx as nx

from simulation import Simulation

def main():
    parser = argparse.ArgumentParser(description='Simulation of spreading of tweets')

    parser.add_argument('--data', default='data/twitter_combined.txt', help='Input data')

    args = parser.parse_args()
    with open(args.data) as input_data:
        G = nx.read_edgelist(input_data)

    simulation = Simulation(G)
    simulation()

if __name__ == "__main__":
    main()