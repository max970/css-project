from os import stat_result
import networkx as nx
from random import choice, sample
from scipy.stats import expon

from enum import Enum

from numpy.core.fromnumeric import size

class Simulation():
    def __init__(self, graph: nx.Graph):
        self.graph = graph

        nx.set_node_attributes(graph, False, "retweeted")
        nx.set_node_attributes(graph, 0, "views")


    def retweet(self, node_label, steps=0):

        stack = [node_label]
        step = 0

        while stack:
            step += 1
            print(step)

            node_label = stack.pop()
            if step > 2500:
                return


            def filter_already_retweeted(node_label):
                node = self.graph.nodes[node_label]
                return not node["retweeted"]

            def filter_interest(inputs):
                _, interest = inputs
                return interest > 0.02

            node = self.graph.nodes[node_label]

            node["retweeted"] = True

            connected_nodes = self.graph.neighbors(node_label)
            connected_nodes = [n for n in connected_nodes]
            
            n_retweets = int(round(len(connected_nodes) * 0.05))
            n_viewed = int(round(len(connected_nodes)) * 0.1)

            viewed_nodes = sample(connected_nodes, n_viewed)
            for viewed_node in viewed_nodes:
                node = self.graph.nodes[viewed_node]
                node["views"] = node["views"] + 1

            retweet_nodes = sample(connected_nodes, n_retweets)
            retweet_nodes = list(filter(filter_already_retweeted, retweet_nodes))

            retweet_nodes_prob = [expon.pdf(self.graph.nodes()[n]["views"]) for n in retweet_nodes]
            retweet_nodes = list(filter(filter_interest, zip(connected_nodes, retweet_nodes_prob)))

            retweet_nodes = [n for n, _ in retweet_nodes]

            # print(len(retweet_nodes))

            for node in retweet_nodes:
                stack.insert(0, node)

            # print(len(stack))s

    def surrounding_nodes(self, node):
        return self.graph.neighbors(node)

    def build_subgraph(self, retweeted_nodes):
        nodes = []
        for node in retweeted_nodes:
            nodes.append(node)
            # nodes.extend(self.surrounding_nodes(node))

        nodes = set(nodes)

        subgraph = self.graph.subgraph(nodes)
        nx.write_gml(subgraph, "subgraph.gml")


    def __call__(self, *args, **kwds):
        def filter_viewed(node_label):
            node = self.graph.nodes[node_label]
            return node["views"] > 0

        def filter_retweeted(node_label):
            node = self.graph.nodes[node_label]
            return node["retweeted"]

        start_node_label = choice(list(self.graph.nodes()))
        self.retweet(start_node_label)

        viewed_nodes = list(filter(filter_viewed, self.graph.nodes))
        retweet_nodes = list(filter(filter_retweeted, self.graph.nodes))
        print("The tweet reached {} of {} users, {} retweets".format(len(viewed_nodes), len(self.graph.nodes()), len(retweet_nodes)))

        self.build_subgraph(retweet_nodes)
